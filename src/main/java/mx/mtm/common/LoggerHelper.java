package mx.mtm.common;

import org.slf4j.Logger;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

/**
 * @author kirk
 * @since 2015.10.28
 */
public class LoggerHelper {
    /**
     * Log level
     */
    public static enum Level {
        TRACE, DEBUG, INFO, WARN, ERROR
    }

    /**
     * Wrapper to slf4j Logger
     *
     * @param logger  {@link org.slf4j.Logger}
     * @param level   Log level {@link LoggerHelper.Level}
     * @param message log message
     * @param args    array of objects
     */
    public static void log(Logger logger, Level level, String message, Object... args) {
        FormattingTuple ft = format(message, args);
        switch (level) {
            case TRACE:
                logger.trace(ft.getMessage(), ft.getThrowable());
                break;
            case DEBUG:
                logger.debug(ft.getMessage(), ft.getThrowable());
                break;
            case INFO:
                logger.info(ft.getMessage(), ft.getThrowable());
                break;
            case WARN:
                logger.warn(ft.getMessage(), ft.getThrowable());
                break;
            case ERROR:
                logger.error(ft.getMessage(), ft.getThrowable());
                break;
        }
    }

    /**
     * format message
     *
     * @param message string to format
     * @param args    array of objects
     * @return FormattingTuple object
     */
    public static FormattingTuple format(String message, Object... args) {
        FormattingTuple ft = MessageFormatter.arrayFormat(message, args);
        String encoded;
        if (ft.getMessage() != null && !ft.getMessage().isEmpty()) {
            encoded = ft.getMessage().
                replaceAll("\r", "_").
                replaceAll("\\\\r", "_").
                replaceAll("\n", "_").
                replaceAll("\\\\n", "_");
//            encoded = ESAPI.encoder().encodeForOS(new LoggerCodec(), encoded);
            if (ft.getArgArray() == null || ft.getArgArray().length == 0) {

                return MessageFormatter.arrayFormat(
                    encoded,
                    new Object[]{ft.getThrowable()}
                );
            }
            return new FormattingTuple(
                encoded,
                ft.getArgArray(),
                ft.getThrowable()
            );
        } else {
            return ft;
        }
    }
}
